# workflow-cutout


## Description
Trial repo for storing workflows using the cutout as an example to populate a skeleton outlay


## Installation
Ideally the user should be able to find the workflow for their needs, clone, tweak inputs and parameters, and run on their data

## Usage
Browse, clone and follow steps to tweak and run

## Support
Good point as there has to support for each the different workflows and well also for this repo

## Roadmap
To be incorporated into SKA regional centres

## Contributing
Want people to add there workflows

## Authors and acknowledgment
In the end many people hopefully

## License
Licences for each workflow

## Project status
Just started
